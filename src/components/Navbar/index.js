import React from 'react';
import { Menu, Avatar, Col, Row } from 'antd'
import { useNavigate } from 'react-router-dom'
import { signOut } from 'firebase/auth'
import {auth} from "../../utils/firebase";
import './index.scss'



const Navbar = ({ user }) => {
  const navigate = useNavigate();
  const items = [
    {label: 'Chat', key: 'chat'},
    {label: 'Logout', key: 'logout'},
  ]

  const handleClick = async({key}) => {
    if (key === 'logout') {
      await signOut(auth)
      return navigate('/login')
    }
  }
  return (
    <Row className='navbar_wrapper'>
      <Menu
        items={items}
        mode='horizontal'
        selectable={false}
        onClick={handleClick}
      />
      <Col>
        <Avatar src={user.photoURL}/>
      </Col>
    </Row>
  );
};

export default Navbar;
