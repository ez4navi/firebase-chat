import React from 'react';
import {Route, Routes, Navigate} from "react-router-dom";
import { privateRoutes, publicRoutes } from "../../routes";
import { routes } from '../../utils/consts'
import { auth } from "../../utils/firebase";
import {useAuthState} from "react-firebase-hooks/auth";

const AppRouter = () => {
  const [user] = useAuthState(auth)
  return user ? (
    <Routes>
      {privateRoutes.map(({path, element}) =>
        <Route key={path} path={path} element={element} exact/>
      )}
      <Route path="*" element={<Navigate to={routes.CHAT_ROUTE} replace/>} />
    </Routes>
      ) : (
    <Routes>
      {publicRoutes.map(({path, element}) =>
        <Route key={path} path={path} element={element} exact />
      )}
      <Route path="*" element={<Navigate to={routes.LOGIN_ROUTE} replace/>} />
    </Routes>
  )
};

export default AppRouter;
