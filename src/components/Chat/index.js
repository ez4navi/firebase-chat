import React, {useState} from 'react';
import { auth, firestore } from "../../utils/firebase";
import './index.scss'
import {Avatar, Button, Col, Input, Row} from "antd";
import {useCollectionData} from "react-firebase-hooks/firestore";
import { serverTimestamp, collection, addDoc, orderBy, query } from 'firebase/firestore';
import Loader from "../Loader";
import {useAuthState} from "react-firebase-hooks/auth";

const Chat = () => {
  const [value, setValue] = useState('')
  const [user] = useAuthState(auth)
  const [messages, loading] = useCollectionData(query(collection(firestore, 'messages'), orderBy('created', 'desc')))
  const handleChangeValue = (e) => {
    setValue(e.target.value)
  }

  const sendMessage = async () => {
    await addDoc(collection(firestore, 'messages'),{
      uid: user.uid,
      displayName: user.displayName,
      photoURL: user.photoURL,
      text: value,
      created: serverTimestamp()
    })
    setValue('');
  }

  if (loading) {
    return <Loader />
  }

  return (
    <div className='chat_wrapper'>
      <div className='messages_wrapper'>
        {messages.map((message) =>
        <div className={`message_wrapper ${message.uid === user.uid ? 'own_message' : ''}`}>
          <div>
            <div className='message_title'>
              <Avatar src={message.photoURL} />
              <span className='message_name'>{message.displayName}</span>
            </div>
            <p className='message_text'>{message.text}</p>
          </div>
        </div>)}
      </div>

      <Row className='chat-input_wrapper'>
        <Col span={21}>
          <Input value={value} onPressEnter={sendMessage} onChange={handleChangeValue}/>
        </Col>
        <Col span={3}>
          <Button disabled={!value} onClick={sendMessage} className='button'>Send</Button>
        </Col>
      </Row>
    </div>
  );
};

export default Chat;
