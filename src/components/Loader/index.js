import React from 'react';
import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';
import './index.css'


const Loader = () => {
  const antIcon = <LoadingOutlined style={{ fontSize: 40 }} spin />;

  return (
    <div className='loader_wrapper'>
      <Spin indicator={antIcon} />
    </div>
  );
};

export default Loader;
