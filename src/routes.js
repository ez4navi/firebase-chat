import { routes } from './utils/consts'
import Login from "./layouts/Login";
import Chat from "./components/Chat";

export const publicRoutes = [
  {
    path: routes.LOGIN_ROUTE,
    element: <Login />
  }
]

export const privateRoutes = [
  {
    path: routes.CHAT_ROUTE,
    element: <Chat />
  }
]
