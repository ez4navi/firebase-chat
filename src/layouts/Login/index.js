import React from 'react';
import { Layout, Button} from "antd";
import './index.css'
import { GoogleAuthProvider, signInWithPopup } from 'firebase/auth'
import { auth } from "../../utils/firebase";

const { Content } = Layout

const LoginLayout = () => {
  const handleLogin = async () => {
    const provider = new GoogleAuthProvider()
    // eslint-disable-next-line
    const { user } = await signInWithPopup(auth, provider)
  }
  return (
    <Layout className={'login_layout'}>
      <Content className={'login_content'}>
            <Button onClick={handleLogin}>Login with Google Account</Button>
      </Content>
    </Layout>
  );
};

export default LoginLayout;
