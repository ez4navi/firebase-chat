import React from 'react';
import { Layout } from "antd";
import Navbar from "../../components/Navbar";
import './index.css'

const { Header, Content } = Layout

const MainLayout = ({ children, user }) => {

  return (
    <Layout>
      <Header className='header'>
        <Navbar user={user}/>
      </Header>
      <Layout>
        <Content className="site-layout">
          { children }
        </Content>
      </Layout>
    </Layout>
  );
};

export default MainLayout;
