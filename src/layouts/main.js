import React from 'react';
import AppLayout from "./AppLayout";
import LoginLayout from "./Login";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "../utils/firebase";

const MainLayout = ({ children }) => {
  const [user] = useAuthState(auth)
  return user ? (
      <AppLayout user={user}>
        {children}
      </AppLayout>
    ) : (
      <LoginLayout />
    );
};

export default MainLayout;
