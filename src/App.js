import {BrowserRouter} from "react-router-dom";
import AppRouter from "./components/AppRouter";
import './App.css';
import MainLayout from "./layouts/main";
import {useAuthState} from "react-firebase-hooks/auth";
import {auth} from "./utils/firebase";
import Loader from "./components/Loader";

const App = () => {
  // eslint-disable-next-line
  const [user, loading] = useAuthState(auth);

  if (loading) {
    return <Loader />
  }

  return (
   <BrowserRouter>
       <MainLayout>
         <AppRouter />
       </MainLayout>
   </BrowserRouter>
  )
}
export default App;
